package net.sssubtlety.automated_crafting;

import com.google.common.collect.Streams;
import net.fabricmc.fabric.api.transfer.v1.item.ItemVariant;
import net.fabricmc.fabric.api.transfer.v1.storage.Storage;
import net.fabricmc.fabric.api.transfer.v1.storage.base.CombinedStorage;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.RecipeType;
import net.minecraft.registry.HolderLookup;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.sssubtlety.automated_crafting.gui.AutoCrafterGuiDescription;
import net.sssubtlety.automated_crafting.inventory.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static net.sssubtlety.automated_crafting.AutomatedCrafting.LOGGER;

/*
  Not actually a sided inventory, but needs `getAvailableSlots`, `canInsert`, `canExtract`.
  These allow for separate internal inventories and configs.
*/
public class AutoCrafterBlockEntity extends LootableContainerBlockEntity implements SidedInventory, TrimmableInventory, NamedScreenHandlerFactory {
    public static final int MAX_STACK_SIZE = 1;
    public static final int[] AVAILABLE_INDICES;
    public static final Validator validator = new Validator();
    public static final Text NAME = Text.translatable("block.automated_crafting.auto_crafter");

    public static boolean templatePredicate(ItemStack inputStack, ItemStack templateStack) {
        return templateStack.isOf(inputStack.getItem()) && ItemStack.itemsAndComponentsMatch(templateStack, inputStack);
    }

    public static Storage<ItemVariant> getStorage(AutoCrafterBlockEntity blockEntity, Direction direction) {
        if (direction == Direction.DOWN) return blockEntity.inputAndOutputStorage;
        else return blockEntity.inputInventory.storage;
    }

    static {
        // An array of indices of slots that can be interacted with using automation,
        //   in the order they should be interacted with
        AVAILABLE_INDICES = new int[Slots.INPUT_PLUS_OUTPUT_SIZE];
        // pull from output first
        AVAILABLE_INDICES[0] = Slots.OUTPUT_SLOT;
        for (int i = 1; i < Slots.INPUT_PLUS_OUTPUT_SIZE; i++) {
            // PRE_FIRST_INPUT_SLOT because we're starting at i = 1
            AVAILABLE_INDICES[i] = i + Slots.LAST_TEMPLATE_SLOT;
        }
    }


//    protected final DefaultedStackView combinedStacks;
    protected final TemplateInventory templateInventory;
    protected final InputInventory inputInventory;
    protected final SingleOutputInventory output;

    protected final CombinedStorage<ItemVariant, Storage<ItemVariant>> inputAndOutputStorage;
//    protected final CombinedStorage<ItemVariant, Storage<ItemVariant>> combinedStorage;


    protected final Validator.Validation validation;
    protected CraftingRecipe recipeCache;

    public AutoCrafterBlockEntity(BlockPos pos, BlockState state) {
        super(Registrar.BLOCK_ENTITY_TYPE, pos, state);

//        this.combinedStacks = new DefaultedStackView(Slots.INVENTORY_SIZE);
        this.templateInventory = new TemplateInventory(new DefaultedStackView(CraftingView.Grid.SIZE));//this.combinedStacks.subList(Slots.TEMPLATE_START, Slots.INPUT_START));
        this.inputInventory = new InputInventory(new DefaultedStackView(CraftingView.Grid.SIZE), templateInventory, this::tryCraftContinuously);//this.combinedStacks.subList(Slots.INPUT_START, Slots.OUTPUT_SLOT), this.templateInventory);
        this.output = new SingleOutputInventory(this::tryCraftContinuously);

        this.inputAndOutputStorage = new CombinedStorage<>(List.of(output.storage, inputInventory.storage));
//        combinedStorage = new CombinedStorage<>(List.of(
//            InventoryStorage.of(templateInventory, null),
//            InventoryStorage.of(inputInventory, null),
//            InventoryStorage.of(output, null)
//        ));

        this.recipeCache = null;
        this.validation = validator.getValidation();
    }

    protected boolean misMatchesTemplate(int slot, ItemStack inputStack) {
        ItemStack templateStack = this.templateInventory.getStack(slot);
        return !templatePredicate(inputStack, templateStack);
    }

    @Override
    public int getMaxCountPerStack() {
        return MAX_STACK_SIZE;
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        if (slot > Slots.OUTPUT_SLOT) return false;
        else if (slot == Slots.OUTPUT_SLOT) return true;
        else if (slot >= Slots.INPUT_START) return inputInventory.isValid(Slots.toInputSlot(slot), stack);
        else return templateInventory.isValid(slot, stack);
    }

    @Override
    public int count(Item item) {
        int count = templateInventory.count(item);
        count += inputInventory.count(item);
        if (output.getItem() == item) count += output.getCount();
        return count;
    }

    @Override
    public boolean containsAny(Set<Item> items) {
        return  items.contains(output.getItem()) ||
                templateInventory.containsAny(items) ||
                inputInventory.containsAny(items);
    }

    // Serialize the BlockEntity
    @Override
    public void writeNbt(NbtCompound nbt, HolderLookup.Provider lookupProvider) {
        super.writeNbt(nbt, lookupProvider);
        DefaultedList<ItemStack> invStackList = method_11282();
        Inventories.writeNbt(nbt, invStackList, lookupProvider);
    }

    // Deserialize the BlockEntity
    @Override
    public void method_11014(NbtCompound nbt, HolderLookup.Provider lookupProvider) {
        super.method_11014(nbt, lookupProvider);
        DefaultedList<ItemStack> invStackList = DefaultedList.ofSize(Slots.INVENTORY_SIZE, ItemStack.EMPTY);
        Inventories.readNbt(nbt, invStackList, lookupProvider);
        method_11281(invStackList);
    }

    public void tryCraft() {
        Optional<CraftingRecipe> optRecipe = getRecipe();
        if(optRecipe.isPresent()) {
            final var recipe = optRecipe.get();
            assert this.world != null;
            final var output = recipe.craft(templateInventory, this.world.getRegistryManager());
            final var outputAction = checkOutput(output);
            if (outputAction != OutputAction.FAIL) {
                final var remainingStacks = recipe.getRemainder(this.inputInventory);
                ItemStack slotRemainder;
                for (int i = 0; i < CraftingView.Grid.SIZE; i++) {
                    slotRemainder = remainingStacks.get(i);
                    if (slotRemainder.isEmpty())
                        // decrement stack
                        this.inputInventory.removeStack(i, 1);
                    else
                        // set remainder
                        this.inputInventory.setStack(i, slotRemainder);
                }

                if (outputAction == OutputAction.SET)
                    this.output.set(output);
                else //outputAction == OutputAction.INCREMENT
                    this.output.get().increment(output.getCount());
            } else tryPlayFailSound();
        } else tryPlayFailSound();
    }

    protected void tryPlayFailSound() {
        if (world != null && !FeatureControl.shouldCraftContinuously())
            world.syncWorldEvent(1001, pos, 0);
    }

    protected OutputAction checkOutput(ItemStack output) {
        if (this.output.isEmpty()) return OutputAction.SET;
        else if (
            ItemStack.itemsAndComponentsMatch(output, this.output.get()) &&
            this.output.getMaxCount() >= this.output.getCount() + output.getCount()
        ) {
            // outputs can combine and output can fit in stack
            return OutputAction.INCREMENT;
        } else return OutputAction.FAIL;
    }

    protected Optional<CraftingRecipe> getRecipe() {
        if (world == null) {
            LOGGER.error("Trying to get recipe before world is initialized!");
            return Optional.empty();
        }

        if (FeatureControl.isSimpleMode() && inputMisMatchesTemplate()) return Optional.empty();

        if(
            recipeCache == null ||
            validation.invalid() ||
            !recipeCache.matches(inputInventory, world)
        ) {
            recipeCache = this.world.getRecipeManager().getFirstMatch(RecipeType.CRAFTING, inputInventory, this.world)
                .map(RecipeHolder::value).orElse(null);
        }

        return Optional.ofNullable(recipeCache);
    }

    private boolean inputMisMatchesTemplate() {
        for (int slot = 0; slot < CraftingView.Grid.SIZE; slot++)
            if (misMatchesTemplate(slot, inputInventory.getStack(slot))) return true;

        return false;
    }

    protected void tryCraftContinuously() {
        if (
            FeatureControl.shouldCraftContinuously() &&
            world != null &&
            world.getBlockState(pos).get(AutoCrafterBlock.POWERED)
        ) tryCraft();
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        return AVAILABLE_INDICES;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, Direction dir) {
        // this is meant ot be called after isValid, so slot should be empty and match template (if simple)
        int inputSlot = Slots.toInputSlot(slot);
        return CraftingView.Grid.contains(inputSlot);
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        if (slot == Slots.OUTPUT_SLOT || !FeatureControl.isSimpleMode()) return true;

        int inputSlot = Slots.toInputSlot(slot);
        return CraftingView.Grid.contains(inputSlot) && misMatchesTemplate(inputSlot, stack);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        final ItemStack removedStack;
        if (slot == Slots.OUTPUT_SLOT) removedStack = output.split(amount);
        else {
            removedStack = slot >= Slots.INPUT_START ?
                inputInventory.removeStack(Slots.toInputSlot(slot), amount) :
                templateInventory.removeStack(slot, amount);
        }

        return removedStack;
    }

    @Override
    public ItemStack removeStack(int slot) {
        final ItemStack removedStack;
        if (slot == Slots.OUTPUT_SLOT) {
            removedStack = output.get();
            output.set(ItemStack.EMPTY);
        } else {
            removedStack = slot >= Slots.INPUT_START ?
                inputInventory.removeStack(Slots.toInputSlot(slot)) :
                templateInventory.removeStack(slot);
        }

        tryCraftContinuously();
        return removedStack;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        setStackWithoutCrafting(slot, stack);
        tryCraftContinuously();
    }

    protected void setStackWithoutCrafting(int slot, ItemStack stack) {
        // Calling super.setStack() would result in the output slot's contents being truncated
        // So we have to directly set the slot's contents
        if (slot == Slots.OUTPUT_SLOT) output.set(stack);
        else {
            if (slot >= Slots.INPUT_START) inputInventory.setStack(Slots.toInputSlot(slot), stack);
            else templateInventory.setStack(slot, stack);
        }

        markDirty();
    }

    // getInvStackList
    @Override
    protected DefaultedList<ItemStack> method_11282() {
        final DefaultedList<ItemStack> stacks = DefaultedList.of();
        stacks.addAll(templateInventory.getStacks());
        stacks.addAll(inputInventory.getStacks());
        stacks.add(output.get());
        return stacks;
    }

    // setInvStackList
    @Override
    protected void method_11281(DefaultedList<ItemStack> list) {
        final int size = list.size();
        int i;
        for (i = 0; i < Slots.INPUT_START; i++)
            templateInventory.setStack(i, i < size ? list.get(i) : ItemStack.EMPTY);

        for (int iInput = 0; i < Slots.OUTPUT_SLOT; iInput++, i++)
            inputInventory.setStack(iInput, i < size ? list.get(i) : ItemStack.EMPTY);

        output.set(i < size ? list.get(i) : ItemStack.EMPTY);
    }

    @Override
    protected Text getContainerName() {
        return NAME;
    }

    @Override
    public ScreenHandler createScreenHandler(int syncId, PlayerInventory playerInventory) {
        return AutoCrafterGuiDescription.create(syncId, playerInventory, ScreenHandlerContext.create(world, pos));
    }

    @Override
    public int size() {
        return Slots.INVENTORY_SIZE;
    }

    @Override
    public boolean isEmpty() {
        return output.isEmpty() && templateInventory.isEmpty() && inputInventory.isEmpty();
    }

    @Override
    public ItemStack getStack(int slot) {
        if (slot == Slots.OUTPUT_SLOT) return output.get();
        else if (slot >= Slots.INPUT_START) return inputInventory.getStack(Slots.toInputSlot(slot));
        else return templateInventory.getStack(slot);
    }

    @Override
    public void clear() {
        templateInventory.clear();
        inputInventory.clear();
        output.set(ItemStack.EMPTY);
    }

    public int getComparatorOutput() {
        if (FeatureControl.shouldComparatorReadOutput() && !output.isEmpty()) return 15;
        return inputInventory.getComparatorOutput();
    }

    @Override
    public Inventory getTrimmed() {
        return new ArrayInventory(getTrimmedStream());
    }

    @Override
    public Stream<ItemStack> getTrimmedStream() {
        // don't include template because it's a 'ghost' inventory
        Stream<ItemStack> trimmedInput = inputInventory.getTrimmedStream();
        if (output.isEmpty()) return trimmedInput;
        else return Streams.concat(trimmedInput, Stream.of(output.get()));
    }

    public enum OutputAction {
        FAIL, SET, INCREMENT
    }

    public interface Slots {
        int TEMPLATE_START = 0;
        int INPUT_START = CraftingView.Grid.SIZE;
        int LAST_TEMPLATE_SLOT = INPUT_START - 1;

        int INPUT_PLUS_OUTPUT_SIZE = CraftingView.Grid.SIZE + 1;

        int OUTPUT_SLOT = CraftingView.Grid.SIZE * 2;
        int INVENTORY_SIZE = OUTPUT_SLOT + 1;

        static int toInputSlot(int slot) {
            return slot - CraftingView.Grid.SIZE;
        }
    }
}
